// @flow

import * as React from "react";
import { NavLink, withRouter } from "react-router-dom";

import { Site } from "tabler-react";

type Props = {|
  +children: React.Node,
|};

type subNavItem = {|
  +value: string,
  +to?: string,
  +icon?: string,
  +LinkComponent?: React.ElementType,
|};

type navItem = {|
  +value: string,
  +to?: string,
  +icon?: string,
  +active?: boolean,
  +LinkComponent?: React.ElementType,
  +subItems?: Array<subNavItem>,
|};

const navBarItems: Array<navItem> = [
  { value: "Dashboard", to: "/", icon: "home", LinkComponent: withRouter(NavLink) },
  { value: "Bookings", to: "/bookings", icon: "bookmark", LinkComponent: withRouter(NavLink) },
  {
    value: "Patients",
    to: "/patients",
    icon: "users",
    LinkComponent: withRouter(NavLink),
  },
  {
    icon: "settings",
    value: "Settings",
    subItems: [
      { value: "Doctors", to: "/doctors", LinkComponent: withRouter(NavLink) },
      { value: "Services", to: "/services", LinkComponent: withRouter(NavLink) },
      { value: "Others...", to: "/settings", LinkComponent: withRouter(NavLink) },
    ],
  },
];

const notificationsObjects = [
  {
    avatarURL: "./images/default-avatar.png",
    message: (
      <React.Fragment>
        <strong>Nathan</strong> pushed new commit: Fix page load performance
        issue.
      </React.Fragment>
    ),
    time: "10 minutes ago",
  },
  {
    avatarURL: "./images/default-avatar.png",
    message: (
      <React.Fragment>
        <strong>Alice</strong> started new task: Tabler UI design.
      </React.Fragment>
    ),
    time: "1 hour ago",
  },
  {
    avatarURL: "./images/default-avatar.png",
    message: (
      <React.Fragment>
        <strong>Rose</strong> deployed new version of NodeJS REST Api // V3
      </React.Fragment>
    ),
    time: "2 hours ago",
  },
];

const accountDropdownProps = {
  avatarURL: "./images/default-avatar.png",
  name: "Jane Pearson",
  description: "Administrator",
  options: [
    { icon: "user", value: "Profile" },
    { icon: "settings", value: "Settings" },
    { icon: "mail", value: "Inbox", badge: "6" },
    { icon: "send", value: "Message" },
    { isDivider: true },
    { icon: "help-circle", value: "Need help?" },
    { icon: "log-out", value: "Sign out" },
  ],
};

class SiteWrapper extends React.Component<Props, void> {
  render(): React.Node {
    return (
      <Site.Wrapper
        headerProps={{
          href: "/",
          alt: "Dashboard",
          imageURL: "./images/logonhakhoatrang.png",
          navItems: '',
          notificationsTray: { notificationsObjects },
          accountDropdown: accountDropdownProps,
        }}
        navProps={{ itemsObjects: navBarItems }}
        footerProps={{
          copyright: 
          (
            <React.Fragment>
              Copyright © 2018
              <a href="."></a>. Developed by
              <a
                href="https://zodinet.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                {" "}
                Zodinet
              </a>{" "}
            </React.Fragment>
          ),
          nav: '',
        }}
      >
        {this.props.children}
      </Site.Wrapper>
    );
  }
}

export default SiteWrapper;

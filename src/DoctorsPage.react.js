// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  StampCard,
  Form
} from "tabler-react";

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';
import moment from 'moment';

class Doctors extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			doctors: [],
			filteredDoctors: [],
			searchText: ''
		};
	}

	componentDidMount() {
		this.getDoctors();
	}

	componentWillUnmount() {
		this.refDoctors = null;
	}

	getDoctors(callback) {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refDoctors = firestore.collection('doctors');

		var self = this;
		this.refDoctors
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let doctors = [data, ...self.state.doctors];
					
					self.setState({
						doctors
					});

					console.log(doc.id, " => ", data);
				});
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	onRemoveDoctor(event, doctor) {
		this.refDoctors.doc(doctor.id).delete();

		let index = _.findIndex(this.state.doctors, { 'id': doctor.id });
		let oldDoctors =  this.state.doctors;
		oldDoctors.splice(index, 1);
		this.setState({
			doctors: oldDoctors 
		});
	}

	onSearchDoctor() {
		let searchText = this.state.searchText;
		
		var results = _.filter(this.state.doctors, function(item){
			return item.name.indexOf(searchText) > -1;
		});

		this.setState({
			filteredDoctors: results
		});

	}

	renderDoctorItem(key, doctor) {

		return (
			{
				key: key,
				item: [
						{
							content: (
								<Text RootComponent="span" muted>{ key + 1 }</Text>
							),
						},
						{
							content: (
								<Text>{ doctor.name }</Text>
							),
						},
						{ 
							content: (
								<Text>{ doctor.phone }</Text>
							),
						},
						{ 
							content: (
								<Text>{ doctor.title }</Text>
							),
						},
						{ 
							content: (
								<Text>{ doctor.address }</Text>
							),
						},
					
						{
							alignContent: "right",
							content: (
								<React.Fragment> 
									<div className="dropdown">
										<a href={'/editdoctor/' + doctor.id} className="btn btn-sm btn-info edit-button">
											Edit
										</a>
										<Button
											color="danger"
											size="sm"
											onClick={(event) => this.onRemoveDoctor(event, doctor)}
										>
											Remove
										</Button>
									</div>
								</React.Fragment>
							),
						}
					]
			}
		);
	}

	renderDoctorItems() {
		let listDoctors = [];
		let doctors = this.state.filteredDoctors.length != 0 ? this.state.filteredDoctors : this.state.doctors;

		doctors.map((item, key) => {
			let doctorItem = this.renderDoctorItem(key, item);
			listDoctors.push(doctorItem);
		})
 
		return listDoctors;
	}
  
	render()
	{
		let doctorItems = this.renderDoctorItems();

		return (
			<SiteWrapper>
				<Page.Content title="Settings">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Doctors">
								<Grid.Row>
									<Grid.Col md={6} lg={4}> 
										<Form.Group className="search-booking-by-phone">
											<Form.InputGroup> 
												<Form.Input onChange={(event) => this.setState({ searchText: event.target.value }) } placeholder="Search doctor by name..." />
													<Form.InputGroupAppend>
														<Button
															color="primary"
															onClick={() => this.onSearchDoctor()}
															>
															Search!
														</Button>
													</Form.InputGroupAppend>
											</Form.InputGroup>
										</Form.Group>
									</Grid.Col>
									<Grid.Col md={6} lg={8}>
										<a href="/adddoctor" className="btn btn-primary add-button">Add Doctor</a>
									</Grid.Col>
								</Grid.Row>
								<Table
									responsive
									className="card-table table-vcenter text-nowrap"
									headerItems={[
										{ content: "No.", className: "w-1" },
										{ content: "Name" },
										{ content: "Phone Number" },
										{ content: "Title" },
										{ content: "Address" },
										{ content: null }
									]}
									bodyItems={ doctorItems }
								/>
							</Card>
						</Grid.Col>
					</Grid.Row>
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default Doctors;

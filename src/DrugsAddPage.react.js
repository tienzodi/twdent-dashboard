// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  Form
} from "tabler-react";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';

class AddDrug extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			name: '',
			brand: '',
			price: ''
		};
	}

	componentDidMount() {
	}

	componentWillUnmount() {
		this.refDrugs = null;
	}

	onSaveDrug() {
		let data = this.state;

		if(data.name.length == 0) {
			toast(' Please enter name of drug', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}

		if(data.price.length == 0) {
			toast('Please enter price of drug', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}

		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refDrugs = firestore.collection('drugs');

		var self = this;
		this.refDrugs.add(data)
			.then(function() {
				console.log("Document successfully written!");


				toast('SUCCESS!!!', {
					position: "top-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});

				self.setState({
					name: '',
					brand: '',
					price: ''
				});

			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
			});
	}
  
	render()
	{

		return (
			<SiteWrapper>
				<Page.Content title="Settings">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Add Drug">
								<Grid.Col md={6} lg={4}> 
									<Form.Group className="search-booking-by-phone">
										<Form.Group label="Name"> 
											<Form.Input  value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })} placeholder="Enter drug name" />			
										</Form.Group>
										<Form.Group label="Brand"> 
											<Form.Input  value={this.state.brand} onChange={(event) => this.setState({ brand: event.target.value })} placeholder="Enter drug brand" />			
										</Form.Group>
										<Form.Group label="Price"> 
											<Form.Input  value={this.state.price} type="number" onChange={(event) => this.setState({ price: event.target.value })} placeholder="Enter drug price" />			
										</Form.Group>
										<Form.InputGroup> 
											<Button
												color="primary"
												onClick={() => this.onSaveDrug()}
												>
												Save
											</Button>
										</Form.InputGroup>
									</Form.Group>
								</Grid.Col> 
							</Card>
						</Grid.Col>
					</Grid.Row>
					<ToastContainer />
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default AddDrug;

// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  Form
} from "tabler-react";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import MaskedInput from 'react-text-mask';

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';

class AddService extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			name: '',
			price: ''
		};
	}

	componentDidMount() {
	}

	componentWillUnmount() {
		this.refServices = null;
	}

	onSaveService() {
		let data = this.state;

		if(data.name.length == 0) {
			toast(' Please enter name of service', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}

		if(data.price.length == 0) {
			toast('Please enter price of service', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}

		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refServices = firestore.collection('services');

		var self = this;
		this.refServices.add(data)
			.then(function() {
				console.log("Document successfully written!");


				toast('SUCCESS!!!', {
					position: "top-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});

				self.setState({
					name: '',
					price: ''
				});

			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
			});
	}
  
	render()
	{

		return (
			<SiteWrapper>
				<Page.Content title="Services">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Add Service">
								<Grid.Col md={6} lg={4}> 
									<Form.Group className="search-booking-by-phone">
										<Form.Group label="Name"> 
											<Form.Input  value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })} placeholder="Enter service name" />			
										</Form.Group>
										<Form.Group label="Price (VNĐ)"> 
											<Form.Input  value={this.state.price} type="number" onChange={(event) => this.setState({ price: event.target.value })} placeholder="Enter service price" />			
										</Form.Group>
										<Form.InputGroup> 
											<Button
												color="primary"
												onClick={() => this.onSaveService()}
												>
												Save
											</Button>
										</Form.InputGroup>
									</Form.Group>
								</Grid.Col> 
							</Card>
						</Grid.Col>
					</Grid.Row>
					<ToastContainer />
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default AddService;

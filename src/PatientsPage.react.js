// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  StampCard,
  Form
} from "tabler-react";

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';
import moment from 'moment';

class Patients extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			users: [],
			filteredUsers: [],
			searchText: ''
		};
	}

	componentDidMount() {
		this.getUsers();
	}

	componentWillUnmount() {
		this.refUsers = null;
	}

	getUsers(callback) {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refUsers = firestore.collection('users');

		var self = this;
		this.refUsers
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let users = [data, ...self.state.users];
					
					self.setState({
						users
					});

					console.log(doc.id, " => ", data);
				});
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	onRemoveUser(event, user) {

	}

	onSearchUser() {
		let searchText = this.state.searchText;
		
		var results = _.filter(this.state.users, function(item){
			return item.phoneNumber.indexOf(searchText) > -1;
		});

		this.setState({
			filteredUsers: results
		});

	}

	renderUserItem(key, user) {

		return (
			{
				key: key,
				item: [
						{
							content: (
								<Text RootComponent="span" muted>{ key + 1 }</Text>
							),
						},
						{
							content: (
								<Text>{ user.fullName }</Text>
							),
						},
						{ 
							content: (
								<Text>{ user.phoneNumber }</Text>
							),
						},
					
						{
							alignContent: "right",
							content: (
								<React.Fragment> 
									<div className="dropdown">
										<Button
											color="danger"
											size="sm"
											onClick={(event) => this.onRemoveUser(event, user)}
										>
											Remove
										</Button>
									</div>
								</React.Fragment>
							),
						}
					]
			}
		);
	}

	renderUserItems() {
		let listUsers = [];
		let users = this.state.filteredUsers.length != 0 ? this.state.filteredUsers : this.state.users;

		users.map((item, key) => {
			let userItem = this.renderUserItem(key, item);
			listUsers.push(userItem);
		})
 
		return listUsers;
	}
  
	render()
	{
		let userItems = this.renderUserItems();

		return (
			<SiteWrapper>
				<Page.Content title="Dashboard">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Patients">
								<Grid.Col md={6} lg={4}> 
									<Form.Group className="search-booking-by-phone">
										<Form.InputGroup> 
											<Form.Input onChange={(event) => this.setState({ searchText: event.target.value }) } placeholder="Search booking by phone..." />
												<Form.InputGroupAppend>
													<Button
														color="primary"
														onClick={() => this.onSearchUser()}
														>
														Search!
													</Button>
												</Form.InputGroupAppend>
										</Form.InputGroup>
									</Form.Group>
								</Grid.Col>
								<Table
									responsive
									className="card-table table-vcenter text-nowrap"
									headerItems={[
										{ content: "No.", className: "w-1" },
										{ content: "Patient Name" },
										{ content: "Phone Number" },
										{ content: null }
									]}
									bodyItems={ userItems }
								/>
							</Card>
						</Grid.Col>
					</Grid.Row>
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default Patients;

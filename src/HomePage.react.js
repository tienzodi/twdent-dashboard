// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  StampCard,
  Form
} from "tabler-react";

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';
import moment from 'moment';

class Home extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			bookings: [],
			users: [],
			filteredBookings: [],
			searchText: ''
		};
	}

	componentDidMount() {
		let self = this;
		this.getUsers(function(){
			self.getBookings();
		});
	}

	componentWillUnmount() {
		this.refBooking = null;
		this.refUsers = null;
	}

	getUsers(callback) {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refUsers = firestore.collection('users');

		var self = this;
		this.refUsers
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let users = [data, ...self.state.users];
					
					self.setState({
						users
					});

					console.log(doc.id, " => ", data);
				});

				callback && callback();
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	getBookings() {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refBooking = firestore.collection('bookings');

		var self = this;
		this.refBooking
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let bookings = [data, ...self.state.bookings];
					self.setState({
						bookings
					});

					console.log(doc.id, " => ", data);
				});
			}) 
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	onApproveBooking(event, booking) {
		let ref = this.refBooking.doc(booking.id);
		ref.set({
			status: 'Approved'
		}, { merge: true });

		let newBookings = this.state.bookings.map(el => (el.id === booking.id ? Object.assign({}, el, { status: 'Approved' }) : el))
		this.setState({
			bookings: newBookings
		});
	}

	onRemoveBooking(event, booking) {
		this.refBooking.doc(booking.id).delete();

		let index = _.findIndex(this.state.bookings, { 'id': booking.id });
		let oldBookings =  this.state.bookings;
		oldBookings.splice(index, 1);
		this.setState({
			bookings: oldBookings
		});
	}

	onSearchBooking() {
		let searchText = this.state.searchText;
		
		var results = _.filter(this.state.users, function(item){
			return item.phoneNumber.indexOf(searchText) > -1;
		});
		
		let users = _.keyBy(results, 'userId');

	
		let filteredBookings = [];
		
		_.forEach(users, (user) => {
			this.state.bookings.map((item, key) => {
				if(user.id == item.userId) {
					filteredBookings.push(item);
				}
			})
		})

		this.setState({
			filteredBookings
		});

	}

	renderBookingStatus(booking) {
		if(booking.status == 'New') {
			return (
				<React.Fragment>
					<span className="status-icon bg-danger" /> { booking.status }
				</React.Fragment>
			);
		}
		if(booking.status == 'Approved') {
			return (
				<React.Fragment>
					<span className="status-icon bg-success" /> { booking.status }
				</React.Fragment>
			);
		}
	}

	renderBookingItem(key, user, booking) {

		return (
			{
				key: key,
				item: [
						{
							content: (
								<Text RootComponent="span" muted>{ key + 1 }</Text>
							),
						},
						{
							content: (
								<Text>{ user.fullName }</Text>
							),
						},
						{ 
							content: (
								<Text>{ user.phoneNumber }</Text>
							),
						},
						{ 
							content: (
								<Text>{ moment(booking.bookingDate.seconds * 1000).format('DD-MM-YYYY HH:mm:ss') }</Text> 
							),
						},
						{ 
							content: (
								<Text>{ booking.bookingLocation }</Text>
							),
						},
						{ 
							content: (
								<Text>{ booking.bookingType }</Text>
							),
						},
						{
							content: (
								this.renderBookingStatus(booking)
							),
						},
						{ 
							content: (
								<Text>{ booking.bookingContent }</Text>
							),
						},
						{
							alignContent: "right",
							content: (
								<React.Fragment> 
									<div className="dropdown">
										<Button
											color="info"
											size="sm"
											onClick={(event) => this.onApproveBooking(event, booking)}
										>
											Approve
										</Button>
										<span>{"   "}</span>
										<Button
											color="danger"
											size="sm"
											onClick={(event) => this.onRemoveBooking(event, booking)}
										>
											Remove
										</Button>
									</div>
								</React.Fragment>
							),
						}
					]
			}
		);
	}

	renderBookingItems() {
		let bookings = [];

		let users = _.keyBy(this.state.users, 'userId');

		let list = this.state.filteredBookings.length != 0 ? this.state.filteredBookings : this.state.bookings;

		list.map((item, key) => {
			let user = users[item.userId];
			let bookingItem = this.renderBookingItem(key, user, item);
			bookings.push(bookingItem);
		})

		return bookings;
	}
  
	render()
	{
		
		let bookingItems = this.renderBookingItems();

		return (
			<SiteWrapper>
				<Page.Content title="Dashboard">
					<Grid.Row cards={true}>
						<Grid.Col sm={6} lg={3}>
							<StampCard
								color="blue"
								icon="dollar-sign"
								header={
								<a>
									132 <small>Sales</small>
								</a>
								}
								footer={"12 waiting payments"}
							/>
						</Grid.Col>
						<Grid.Col sm={6} lg={3}>
							<StampCard
								color="green"
								icon="shopping-cart"
								header={
								<a>
									78 <small>Orders</small>
								</a>
								}
								footer={"32 shipped"}
							/>
						</Grid.Col>
						<Grid.Col sm={6} lg={3}>
							<StampCard
								color="red"
								icon="users"
								header={
								<a>
									1,352 <small>Members</small>
								</a>
								}
								footer={"163 registered today"}
							/>
						</Grid.Col>
						<Grid.Col sm={6} lg={3}>
							<StampCard
								color="yellow"
								icon="message-square"
								header={
								<a>
									132 <small>Comments</small>
								</a>
								}
								footer={"16 waiting"}
							/>
						</Grid.Col>
						<Grid.Col width={12}>
							<Card title="New Bookings">
								<Grid.Col md={6} lg={4}> 
									<Form.Group className="search-booking-by-phone">
										<Form.InputGroup> 
											<Form.Input onChange={(event) => this.setState({ searchText: event.target.value }) } placeholder="Search booking by phone..." />
												<Form.InputGroupAppend>
													<Button
														color="primary"
														onClick={() => this.onSearchBooking()}
														>
														Search!
													</Button>
												</Form.InputGroupAppend>
										</Form.InputGroup>
									</Form.Group>
								</Grid.Col>
								<Table
									responsive
									className="card-table table-vcenter text-nowrap"
									headerItems={[
										{ content: "No.", className: "w-1" },
										{ content: "Patient Name" },
										{ content: "Phone Number" },
										{ content: "Booking Date" },
										{ content: "Booking Location" },
										{ content: "Booking Type" },
										{ content: "Status" },
										{ content: "Note" },
										{ content: null }
									]}
									bodyItems={ bookingItems }
								/>
							</Card>
						</Grid.Col>
					</Grid.Row>
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default Home;

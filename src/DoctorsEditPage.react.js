// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  Form
} from "tabler-react";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';

class EditDoctor extends Component {
	constructor(props) {
		super(props);

		const { match: { params } } = this.props;
		
		this.state = {
			id: params.id,
			name: '',
			phone: '',
			title: '',
			address: ''
		};
	}

	componentDidMount() {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refDoctors = firestore.collection('doctors').doc(this.state.id);

		let self = this;
		self.refDoctors.get().then(function(doc) {
			if (doc.exists) {
				self.setState(doc.data());
			} else {
				// doc.data() will be undefined in this case
				console.log("No such document!");
			}
		}).catch(function(error) {
			console.log("Error getting document:", error);
		});
	}

	componentWillUnmount() {
		this.refDoctors = null;
	}

	onSaveDoctor() {
		let data = this.state;

		if(data.name.length == 0) {
			toast(' Please enter name of doctor', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}

		if(data.phone.length == 0) {
			toast('Please enter phone of doctor', {
				position: "top-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return;
		}
		

		var self = this;
		this.refDoctors.set(data)
			.then(function() {
				console.log("Document successfully written!");


				toast('SUCCESS!!!', {
					position: "top-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
			});
	}
  
	render()
	{

		return (
			<SiteWrapper>
				<Page.Content title="Doctors">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Edit Doctor">
								<Grid.Col md={6} lg={4}> 
									<Form.Group className="search-booking-by-phone">
										<Form.Group label="Name"> 
											<Form.Input  value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })} placeholder="Enter doctor name" />			
										</Form.Group>
										<Form.Group label="Phone"> 
											<Form.Input  value={this.state.phone} type="number" onChange={(event) => this.setState({ phone: event.target.value })} placeholder="Enter doctor phone" />			
										</Form.Group>
										<Form.Group label="Title"> 
											<Form.Input  value={this.state.title} onChange={(event) => this.setState({ title: event.target.value })} placeholder="Enter doctor title" />			
										</Form.Group>
										<Form.Group label="Address"> 
											<Form.Input  value={this.state.address} onChange={(event) => this.setState({ address: event.target.value })} placeholder="Enter doctor address" />			
										</Form.Group>
										<Form.InputGroup> 
											<Button
												color="primary"
												onClick={() => this.onSaveDoctor()}
												>
												Save
											</Button>
										</Form.InputGroup>
									</Form.Group>
								</Grid.Col> 
							</Card>
						</Grid.Col>
					</Grid.Row>
					<ToastContainer />
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default EditDoctor;

import * as React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import {
	LoginPage,
	UserLoginPage,
	Error400,
	Error401,
	Error403,
	Error404,
	Error500,
	Error503,
	SettingPage
} from "./pages";

import HomePage from "./HomePage.react";
import PatientsPage from './PatientsPage.react';

import DrugsPage from './DrugsPage.react';
import DoctorsPage from './DoctorsPage.react';
import ServicesPage from './ServicesPage.react';


import AddDrugPage from './DrugsAddPage.react';
import EditDrugPage from './DrugsEditPage.react.js';
import AddDoctorPage from './DoctorsAddPage.react';
import EditDoctorsPage from './DoctorsEditPage.react';

import AddServicePage from './ServiceAddPage.react';
import EditServicePage from './ServiceEditPage.react';

import BookingsPage from './BookingsPage.react';
import BookingDetailPage from './BookingDetailPage.react';

import firebase from './firebase';

import "tabler-react/dist/Tabler.css";

type Props = {||};

function App(props: Props): React.Node {
	return (
		<React.StrictMode>
		<Router basename="/">
			<Switch>
			<Route exact path="/login" component={LoginPage} />
			<Route exact path="/user-login" component={UserLoginPage} />

			<PrivateRoute exact path="/" component={HomePage} />
			<PrivateRoute exact path="/patients" component={PatientsPage} />
			<PrivateRoute exact path="/settings" component={SettingPage} />

			<PrivateRoute exact path="/drugs" component={DrugsPage} />
			<PrivateRoute exact path="/adddrug" component={AddDrugPage} />
			<PrivateRoute exact path="/editdrug/:id" component={EditDrugPage} />

			<PrivateRoute exact path="/doctors" component={DoctorsPage} />
			<PrivateRoute exact path="/adddoctor" component={AddDoctorPage} />
			<PrivateRoute exact path="/editdoctor/:id" component={EditDoctorsPage} />

			<PrivateRoute exact path="/services" component={ServicesPage} />
			<PrivateRoute exact path="/addservice" component={AddServicePage} />
			<PrivateRoute exact path="/editservice/:id" component={EditServicePage} />

			<PrivateRoute exact path="/bookings" component={BookingsPage} />
			<PrivateRoute exact path="/bookingdetail/:id" component={BookingDetailPage} />

			<Route exact path="/400" component={Error400} />
			<Route exact path="/401" component={Error401} />
			<Route exact path="/403" component={Error403} />
			<Route exact path="/404" component={Error404} />
			<Route exact path="/500" component={Error500} />
			<Route exact path="/503" component={Error503} />
			<Route component={Error404} />
			</Switch>
		</Router>
		</React.StrictMode>
	);
}

const fakeAuth = {
	isAuthenticated: false,
	authenticate(cb) {
		let user = localStorage.getItem('user');
		if(user) {
			this.isAuthenticated = true;
		}

		let self = this;
		firebase.auth().onAuthStateChanged(function(user) {
			self.isAuthenticated = true;
		});
	},
	signout(cb) {
		let user = localStorage.getItem('user');
		if(!user) {
			this.isAuthenticated = false;
		}
	}
};

fakeAuth.authenticate();

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
		fakeAuth.isAuthenticated ? (
			<Component {...props} />
		) : (
			<Redirect
			to={{
				pathname: "/login",
				state: { from: props.location }
			}}
			/>
		)
		}
	/>
);

export default App;

// @flow

import React, { Component } from "react";
import { Formik } from "formik";
import { LoginPage as TablerLoginPage } from "tabler-react";

import { Redirect } from "react-router-dom";

import firebase from '../firebase';

type Props = {||};

class LoginPage extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            redirect: false
        };
    }
    

    render() {
        if(this.state.redirect) {
            return (<Redirect to={{ pathname: "/" }} />);
        }

        let self = this;

        return (
            <Formik
                initialValues={{
                    email: "",
                    password: "",
                }}
                validate={values => {
                    // same as above, but feel free to move this into a class method now.
                    let errors = {};
                    if (!values.email) {
                        errors.email = "Required";
                    } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
                    ) {
                        errors.email = "Invalid email address";
                    }
                    return errors;
                }}
                onSubmit={(
                    values,
                    { setSubmitting, setErrors /* setValues and other goodies */ }
                ) => {
                    // Register a new user
                    // firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
                    //     .catch(function (err) {
                    //         console.log(err);
                    //     });
    
                    firebase.auth().signInWithEmailAndPassword(values.email, values.password)
                        .then((res) => {
                            if(res.user) {
                                localStorage.setItem('user', JSON.stringify(res.user));
                                self.setState({
                                    redirect: true
                                });
                            }
                        })
                        .catch(function(err) {
                            console.log(err);
                        });
                }}
                render={({
                    values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                }) => (
                                <TablerLoginPage
                                    onSubmit={handleSubmit}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    values={values}
                                    errors={errors}
                                    touched={touched}
                                />
                            )}
            />
        );
    }
    
}

export default LoginPage;

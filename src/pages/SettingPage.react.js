// @flow

import React, { Component } from "react";
import { Form, Card, Page, Button, Grid } from "tabler-react";

import firebase from './../firebase.js';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import SiteWrapper from "../SiteWrapper.react";


class SettingPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
			facebook_url: "",
			hotline: "",
			website_url: "",
            zalo: "",
            promotion: '',
			loading: true
        }
    }

    componentDidMount() {
        this.getSettings();
    }

    getSettings() {
        const firestore = firebase.firestore();
        const settings = {/* your settings... */ timestampsInSnapshots: true};
        firestore.settings(settings);

        this.ref = firestore.collection('settings').doc("about_us");

		var self = this;
		this.ref.get().then(function(doc) {
			if (doc.exists) {
				console.log("Document data:", doc.data());
				self.setState(doc.data());
			} else {
				console.log("No such document!");
			}
		}).catch(function(error) {
			console.log("Error getting document:", error);
		});
    }

    onSave(event) {
        event.preventDefault();

        let data = this.state;

        console.log(data);
        
        this.ref.set(data)
        .then(function() {
            console.log("Document successfully written!");
            toast('SUCCESS!!!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });
        
    }

    render() {
        return (
            <SiteWrapper>
                <Page.Card
                    title="App Informations"
                    RootComponent={Form}
                    footer={
                        <Card.Footer>
                            <div className="d-flex">
                                <Button type="submit" color="primary" onClick={(event) => this.onSave(event)} className="ml-auto">
                                    Save
                                </Button>
                            </div>
                        </Card.Footer>
                    }
                >
                    <Grid.Row>
                        <Grid.Col lg={6}>
                            <Form.Group label="Email">
                                <Form.Input
                                    placeholder="Enter your email"
                                    value={this.state.email}
                                    onChange={(event) => { this.setState({ email: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                        <Grid.Col lg={6}>
                            <Form.Group label="Facebook Url">
                                <Form.Input
                                    placeholder="Enter Facebook Url"
                                    value={this.state.facebook_url}
                                    onChange={(event) => { this.setState({ facebook_url: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Col lg={12}>
                            <Form.Group label="Hotline">
                                <Form.Input
                                    placeholder="Enter your Hotline"
                                    value={this.state.hotline}
                                    onChange={(event) => { this.setState({ hotline: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Col lg={12}>
                            <Form.Group label="Website">
                                <Form.Input
                                    placeholder="Enter your website"
                                    value={this.state.website_url}
                                    onChange={(event) => { this.setState({ website_url: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Col lg={12}>
                            <Form.Group label="Zalo">
                                <Form.Input
                                    placeholder="Enter your zalo"
                                    value={this.state.zalo}
                                    onChange={(event) => { this.setState({ zalo: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Col lg={12}>
                            <Form.Group label="Promotion">
                                <Form.Input
                                    placeholder="Enter your promotion text"
                                    value={this.state.promotion}
                                    onChange={(event) => { this.setState({ promotion: event.target.value })}}
                                />
                            </Form.Group>
                        </Grid.Col>
                    </Grid.Row>
                </Page.Card>
                <ToastContainer />
            </SiteWrapper>
        );
    }
}

export default SettingPage;

// NOTE: We should consider a base class here
import config from './config.json';

class api {
    baseUrl = config.api_url;

    objectToQueryString = obj =>
        Object.keys(obj)
            .map(k => `${k}=${obj[k]}`)
            .join("&");

    async makeGetRequest(url) {
        const accessToken = localStorage.getItem('Token');
        return fetch(url, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: accessToken
            }
        }).then(response => response.json());
    };

    async makeRequest(url, body, method, options) {
        const accessToken = localStorage.getItem('Token');
        let headers = {
            Accept: "application/json",
            "Content-Type": "application/json"
        }

        if(accessToken) {
            headers['Authorization'] = accessToken;
        }

        if (options && options.headers) {
            Object.keys(options.headers).forEach(function (key) {
                headers[key] = options.headers[key];
            });
        }
        return fetch(url, {
            method: method,
            headers: headers,
            body: options && options.form ? body : JSON.stringify(body)
        }).then(response => response.json());
    }

    async getAll(controller) {
        return this.makeGetRequest(this.baseUrl + controller);
    }

    async customGET(controller, request, params) {
        let url = this.baseUrl + controller;

        if (!request) {
            request = "";
        }

        if (params) {
            const queryParams = this.objectToQueryString(params);
            request += "?" + queryParams;
        }

        if (request) {
            url += "/" + request;
        }

        console.log(url);
        return this.makeGetRequest(url);
    }

    async customDELETE(controller, request, body) {
        return this.makeRequest(this.baseUrl + controller + '/' + request, body, "DELETE");
    }

    async customPOST(controller, request, body, options) {
        let url = this.baseUrl + controller;
        if (request) {
            url += "/" + request;
        }
        return this.makeRequest(url, body, "POST", options);
    }

    async customPUT(controller, request, id, body) {
        let url = this.baseUrl + controller;
        if (id) {
            url += "/" + id;
        }

        if (request) {
            url += "/" + request;
        }

        return this.makeRequest(url, body, "PUT");
    }
}

export default new api();
// @flow

import * as React from "react";
import { Form, Card, StandaloneFormPage, Button } from "tabler-react";
import ReCAPTCHA from "react-google-recaptcha";
import config from './config.json';

class UserLoginPage extends React.Component {
    constructor(props) {
        super(props);

        var params = new URLSearchParams(props.location.search),
            accountLinkingToken = params.get("account_linking_token"),
            redirectUri = params.get("redirect_uri");

        this.state = {
            phoneNumber: '',
            otp: '',
            accountLinkingToken: accountLinkingToken,
            redirectUri: redirectUri,
            token: null,
            isVerified: false
        }
    }

    confirmPhoneNumber = (event) => {
        event.preventDefault();

        if(!this.state.isVerified) {
            if (this.state.phoneNumber.length !== 0 && this.state.token) {
                fetch(config.api_url + 'accounts/fetchOtp', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        phone: this.state.phoneNumber,
                        'g-recaptcha-response': this.state.token
                    })
                })
                .then(response => response.json())
                .then(response => {
                    if (response.res) {

                        this.setState({'isVerified': true});
                    } else {
                        if(response.error) {
                            alert(`Hệ thống đang có lỗi xin thử lại sau. Error: ${response.error.statusCode} ${response.error.message}`);
                        }
                    }
                });
            } else {
                if (this.state.phoneNumber.length === 0)
                    alert('Vui lòng nhập số điện thoại');
                else if (!this.state.token) {
                    alert('Vui lòng xác nhận bạn không phải là robot');
                }
            }
        } else {
            if (this.state.otp.length !== 0) {
                fetch(config.api_url + 'accounts/userLogin', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        phone: this.state.phoneNumber,
                        otp: this.state.otp,
                        token: this.state.accountLinkingToken
                    })
                })
                .then(response => response.json())
                .then(response => {
                    if (response.res) {
                        let authorization_code = response.res;
                        let callbackUrl = `${this.state.redirectUri}&authorization_code=${authorization_code}`;
                        window.location = callbackUrl;
                    }
                });
            } else {
                alert('Vui lòng nhập mã xác thực');
            }
        }
        
    }

    handleInputChange = (event) => {
        this.setState({'phoneNumber':event.target.value });
    }

    handleOtpChange = (event) => {
        this.setState({'otp':event.target.value });
    }

    captchaChange = (value) => {
        this.setState({'token':value });
        console.log(value);
    }

    render(){
        return (
            <StandaloneFormPage>
                <Form className="card" onSubmit={this.confirmPhoneNumber}>
                    <Card.Body>
                        <Card.Title>Đăng nhập</Card.Title>
                        {!this.state.isVerified &&
                            <div>
                                <Form.Group label="Số điện thoại">
                                    <Form.Input
                                        name="phone-input"
                                        placeholder="Số điện thoại"
                                        type="number"
                                        onChange={this.handleInputChange}
                                    />
                                </Form.Group>
                                <ReCAPTCHA sitekey={config.captcha}
                                    onChange={this.captchaChange}>
                                </ReCAPTCHA>
                                <Form.Footer>
                                    <Button type="submit" color="primary" block={true}>
                                        Gửi
                                    </Button>
                                </Form.Footer>
                            </div>
                        }
                        {this.state.isVerified &&
                            <div>
                                <Form.Group label="Mã OTP">
                                    <Form.Input
                                        name="otp-input"
                                        placeholder="Mã OTP"
                                        type="number"
                                        onChange={this.handleOtpChange}
                                    />
                                </Form.Group>
                                <Form.Footer>
                                    <Button type="submit" color="primary" block={true}>
                                        Xác thực
                                    </Button>
                                </Form.Footer>
                            </div>
                        }
                    </Card.Body>
                </Form>
            </StandaloneFormPage>
        );
    }
}

export default UserLoginPage;

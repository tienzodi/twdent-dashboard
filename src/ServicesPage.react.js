// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  StampCard,
  Form
} from "tabler-react";

import NumberFormat from 'react-number-format';

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';
import moment from 'moment';

class Services extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			services: [],
			filteredServices: [],
			searchText: ''
		};
	}

	componentDidMount() {
		this.getServices();
	}

	componentWillUnmount() {
		this.refServices = null;
	}

	getServices(callback) {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refServices = firestore.collection('services');

		var self = this;
		this.refServices
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let services = [data, ...self.state.services];
					
					self.setState({
						services
					});

					console.log(doc.id, " => ", data);
				});
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	onRemoveService(event, service) {
		this.refServices.doc(service.id).delete();

		let index = _.findIndex(this.state.services, { 'id': service.id });
		let oldServices =  this.state.services;
		oldServices.splice(index, 1);
		this.setState({
			services: oldServices 
		});
	}

	onSearchService() {
		let searchText = this.state.searchText;
		
		var results = _.filter(this.state.services, function(item){
			return item.name.indexOf(searchText) > -1;
		});

		this.setState({
			filteredServices: results
		});

	}

	renderServiceItem(key, service) {

		return (
			{
				key: key,
				item: [
						{
							content: (
								<Text RootComponent="span" muted>{ key + 1 }</Text>
							),
						},
						{
							content: (
								<Text>{ service.name }</Text>
							),
						},
						{  
							content: (
								<NumberFormat value={service.price} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />
							),
						},
					
						{
							alignContent: "right",
							content: (
								<React.Fragment> 
									<div className="dropdown">
										<a href={'/editservice/' + service.id} className="btn btn-sm btn-info edit-button">
											Edit
										</a>
										<Button
											color="danger"
											size="sm"
											onClick={(event) => this.onRemoveService(event, service)}
										>
											Remove
										</Button>
									</div>
								</React.Fragment>
							),
						}
					]
			}
		);
	}

	renderServiceItems() {
		let listServices = [];
		let services = this.state.filteredServices.length != 0 ? this.state.filteredServices : this.state.services;

		services.map((item, key) => {
			let serviceItem = this.renderServiceItem(key, item);
			listServices.push(serviceItem);
		})
 
		return listServices;
	}
  
	render()
	{
		let serviceItems = this.renderServiceItems();

		return (
			<SiteWrapper>
				<Page.Content title="Settings">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Services">
								<Grid.Row>
									<Grid.Col md={6} lg={4}> 
										<Form.Group className="search-booking-by-phone">
											<Form.InputGroup> 
												<Form.Input onChange={(event) => this.setState({ searchText: event.target.value }) } placeholder="Search service by name..." />
													<Form.InputGroupAppend>
														<Button
															color="primary"
															onClick={() => this.onSearchService()}
															>
															Search!
														</Button>
													</Form.InputGroupAppend>
											</Form.InputGroup>
										</Form.Group>
									</Grid.Col>
									<Grid.Col md={6} lg={8}>
										<a href="/addservice" className="btn btn-primary add-button">Add Service</a>
									</Grid.Col>
								</Grid.Row>
								<Table
									responsive
									className="card-table table-vcenter text-nowrap"
									headerItems={[
										{ content: "No.", className: "w-1" },
										{ content: "Name" },
										{ content: "Price" },
										{ content: null }
									]}
									bodyItems={ serviceItems }
								/>
							</Card>
						</Grid.Col>
					</Grid.Row>
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default Services;

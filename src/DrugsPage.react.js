// @flow

import React, { Component } from "react";

import {
  Page,
  Grid,
  Card,
  Text,
  Table,
  Button,
  Form
} from "tabler-react";

import firebase from './firebase';

import SiteWrapper from "./SiteWrapper.react";

import _ from 'lodash';

class Drugs extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			drugs: [],
			filteredDrugs: [],
			searchText: ''
		};
	}

	componentDidMount() {
		this.getDrugs();
	}

	componentWillUnmount() {
		this.refDrugs = null;
	}

	getDrugs(callback) {
		const firestore = firebase.firestore();
		const settings = {/* your settings... */ timestampsInSnapshots: true};
		firestore.settings(settings);

		this.refDrugs = firestore.collection('drugs');

		var self = this;
		this.refDrugs
			.get().then(function(querySnapshot) {
				querySnapshot.forEach(function(doc) {
					
					let data = doc.data();
					data['id'] = doc.id;

					let drugs = [data, ...self.state.drugs];
					
					self.setState({
						drugs
					});

					console.log(doc.id, " => ", data);
				});
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
	}

	onRemoveDrug(event, drug) {
		this.refDrugs.doc(drug.id).delete();

		let index = _.findIndex(this.state.drugs, { 'id': drug.id });
		let oldDrugs =  this.state.drugs;
		oldDrugs.splice(index, 1);
		this.setState({
			drugs: oldDrugs 
		});
	}

	onSearchDrug() {
		let searchText = this.state.searchText;
		
		var results = _.filter(this.state.drugs, function(item){
			return item.phoneNumber.indexOf(searchText) > -1;
		});

		this.setState({
			filteredDrugs: results
		});

	}

	renderDrugItem(key, drug) {
		return (
			{
				key: key,
				item: [
						{
							content: (
								<Text RootComponent="span" muted>{ key + 1 }</Text>
							),
						},
						{
							content: (
								<Text>{ drug.name }</Text>
							),
						},
						{ 
							content: (
								<Text>{ drug.brand }</Text>
							),
						},
						{ 
							content: (
								<Text>{ drug.price }</Text>
							),
						},
						{
							alignContent: "right",
							content: (
								<React.Fragment> 
									<div className="dropdown">
										<Button
											color="danger"
											size="sm"
											onClick={(event) => this.onRemoveDrug(event, drug)}
										>
											Remove
										</Button>
									</div>
								</React.Fragment>
							),
						}
					]
			}
		);
	}

	renderDrugItems() {
		let listDrugs = [];
		let drugs = this.state.filteredDrugs.length != 0 ? this.state.filteredDrugs : this.state.drugs;

		drugs.map((item, key) => {
			let drugItem = this.renderDrugItem(key, item);
			listDrugs.push(drugItem);
		})
 
		return listDrugs;
	}
  
	render()
	{
		let drugItems = this.renderDrugItems();

		return (
			<SiteWrapper>
				<Page.Content title="Settings">
					<Grid.Row cards={true}>
						<Grid.Col width={12}>
							<Card title="Drugs">
								<Grid.Row>
									<Grid.Col md={6} lg={4}> 
										<Form.Group className="search-booking-by-phone">
											<Form.InputGroup> 
												<Form.Input onChange={(event) => this.setState({ searchText: event.target.value }) } placeholder="Search booking by phone..." />
													<Form.InputGroupAppend>
														<Button
															color="primary"
															onClick={() => this.onSearchDrug()}
															>
															Search!
														</Button>
													</Form.InputGroupAppend>
											</Form.InputGroup>
										</Form.Group>
									</Grid.Col>
									<Grid.Col md={6} lg={8}>
										<a href="/adddrug" className="btn btn-primary add-button">Add Drug</a>
									</Grid.Col>
								</Grid.Row>
								<Table
									responsive
									className="card-table table-vcenter text-nowrap"
									headerItems={[
										{ content: "No.", className: "w-1" },
										{ content: "Name" },
										{ content: "Brand" },
										{ content: "Price" },
										{ content: null }
									]}
									bodyItems={ drugItems }
								/>
							</Card>
						</Grid.Col>
					</Grid.Row>
				</Page.Content>
			</SiteWrapper>
		);
  	}
}

export default Drugs;
